package cl.ubb.kata;

import static org.junit.Assert.*;

import org.junit.Test;

public class PrimoTest {
	
	private boolean var;

	@Test
	public void Numero2EsPrimo() {
		
				
		NumeroPrimo num=new NumeroPrimo();
		
		var=num.esPrimo(2);
		
		assertEquals(true,var);
		
		
	}
	
	@Test
	public void Numero3EsPrimo() {
		
			
		NumeroPrimo num=new NumeroPrimo();
		
		var=num.esPrimo(3);
		
		assertEquals(true,var);
		
		
	}
	
	@Test
	public void Numero5EsPrimo() {
		
				
		NumeroPrimo num=new NumeroPrimo();
		
		var=num.esPrimo(5);
		
		assertEquals(true,var);
		
		
	}
	
	@Test
	public void Numero6NoEsPrimo() {
		
				
		NumeroPrimo num=new NumeroPrimo();
		
		var=num.esPrimo(6);
		
		assertEquals(false,var);
		
		
	}
	
	@Test
	public void Numero7EsPrimo() {
		
				
		NumeroPrimo num=new NumeroPrimo();
		
		var=num.esPrimo(7);
		
		assertEquals(true,var);
		
		
	}
	
	@Test
	public void Numero11EsPrimo() {
		
				
		NumeroPrimo num=new NumeroPrimo();
		
		var=num.esPrimo(11);
		
		assertEquals(true,var);
		
		
	}
	
	@Test
	public void NumeroMenos5NoEsPrimo() {
		
				
		NumeroPrimo num=new NumeroPrimo();
		
		var=num.esPrimo(-5);
		
		assertEquals(false,var);
		
		
	}
	
	@Test
	public void NumeroUnoNoEsPrimo() {
		
				
		NumeroPrimo num=new NumeroPrimo();
		
		var=num.esPrimo(1);
		
		assertEquals(false,var);
		
		
	}
	
	
	
	
	
	

}
